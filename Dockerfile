FROM debian
MAINTAINER avimehenwal
LABEL yocto.version="zeus 3.0"

RUN apt-get update && apt-get install -y \
    gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
    xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
    pylint3 xterm \
    vim 

# Run bitbake as root
COPY ./avi-os/meta/conf/sanity.conf /meta/conf/sanity.conf