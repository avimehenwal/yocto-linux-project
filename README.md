# yocto-linux-project

Building embedded Linux using yocto

### Dependencies

```sh
git submodule add -b zeus git://git.yoctoproject.org/poky
git submodule status
```

### Building

```sh
docker-compose build
docker-compose up
docker-compose run yocto bash
```

### Bitbake

```sh
# All references where a environment variable was changed
bitbake --environment virtual/kernel | grep '^PV'

```